-- Records

TRUNCATE TABLE `home_setup`;

------ Update Examination User From Here ------





CREATE TABLE `home_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `image` varchar(512) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `projects_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `image` varchar(512) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `about_us_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `image` varchar(512) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `contact_us_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `image` varchar(512) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `testimonials_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `image` varchar(512) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;























