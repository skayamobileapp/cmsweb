<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login_model (Login Model)
 * Login model class to get to authenticate user credentials 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Applicant_login_model extends CI_Model
{
    
    function loginApplicant($email, $password)
    {
        $this->db->select('stu.id as id_applicant, stu.full_name as applicant_name, stu.email_id, stu.password, stu.email_verified');
        $this->db->from('applicant as stu');
        $this->db->where('stu.email_id', $email);
        // $this->db->where('stu.email_verified', '1');
        // $this->db->where('stu.isDeleted', 0);
        $query = $this->db->get();
        
        $user = $query->row();
        
        if(!empty($user))
        {
        // echo "<Pre>";print_r($user->password);exit();
            if(password_verify($password, password_hash($user->password, PASSWORD_DEFAULT)))
            {
        // echo "<Pre>";print_r($user);exit();
                return $user;
            }else
            {
                return array();
            }
        }
        else
        {
            return array();
        }
    }

    function applicantLastLoginInfo($id_applicant)
    {
        $this->db->select('BaseTbl.created_dt_tm');
        $this->db->where('BaseTbl.id_applicant', $id_applicant);
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('applicant_last_login as BaseTbl');

        return $query->row();
    }


    function checkApplicantEmailExist($email)
    {
        $this->db->select('userId');
        $this->db->where('email', $email);
        $query = $this->db->get('applicant');

        if ($query->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    function resetPasswordUser($data)
    {
        $result = $this->db->insert('tbl_reset_password', $data);

        if($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function getApplicantInfoByEmail($email)
    {
        $this->db->select('id, email_id, full_name');
        $this->db->from('applicant');
        $this->db->where('email_id', $email);
        $query = $this->db->get();

        return $query->row();
    }

    function checkActivationDetails($email, $activation_id)
    {
        $this->db->select('id');
        $this->db->from('tbl_reset_password');
        $this->db->where('email', $email);
        $this->db->where('activation_id', $activation_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function createPasswordUser($email, $password)
    {
        $this->db->where('email', $email);
        $this->db->where('isDeleted', 0);
        $this->db->update('tbl_users', array('password'=>getHashedPassword($password)));
        $this->db->delete('tbl_reset_password', array('email'=>$email));
    }

    function addApplicantLastLogin($loginInfo)
    {
        $this->db->trans_start();
        $this->db->insert('applicant_last_login', $loginInfo);
        $this->db->trans_complete();
    }

    function addNewApplicantRegistration($data)
    {
        $this->db->trans_start();
        $this->db->insert('applicant', $data);
        // $this->db->insert('applicant_registration', $data);
        $this->db->trans_complete();
    }

    function checkDuplicateApplicantRegistration($data)
    {
        $this->db->select('id, email_id, full_name');
        $this->db->from('applicant');
        $this->db->where('email_id', $data['email_id']);
        $this->db->or_where('phone', $data['phone']);
        $this->db->or_where('nric', $data['nric']);
        $query = $this->db->get();
        return $query->row();
    }
}

?>