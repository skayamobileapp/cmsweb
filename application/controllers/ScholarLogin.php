<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class ScholarLogin extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('scholar_login_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        // echo "<Pre>";print_r("dasdsa");exit();
        $this->checkScholarLoggedIn();
    }
    

    function checkScholarLoggedIn()
    {
        $isScholarLoggedIn = $this->session->userdata('isScholarLoggedIn');
        
        if(!isset($isScholarLoggedIn) || $isScholarLoggedIn != TRUE)
        {
            $this->load->view('scholar_login');
        }
        else
        {
            redirect('scholarship/scheme/list');
        }
    }


    public function scholarLogin()
    {
        $formData = $this->input->post();
        // echo "<Pre>"; print_r($formData);exit;
        $domain = $this->getDomainName();

        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[128]|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $email = strtolower($this->security->xss_clean($this->input->post('email')));
            $password = $this->input->post('password');
            
            $result = $this->scholar_login_model->loginScholar($email, $password);
            
            
            if(!empty($result))
            {
                
                    
                $lastLogin = $this->scholar_login_model->scholarLastLoginInfo($result->id);

                if($lastLogin == '')
                {
                    $scholar_login = date('Y-m-d h:i:s');
                }
                else
                {
                    $scholar_login = $lastLogin->created_dt_tm;
                }

                $sessionArray = array('id_scholar'=>$result->id,
                                        'scholar_name'=>$result->name,
                                        'scholar_role'=>$result->id_role,
                                        'scholarship_last_login'=> $lastLogin->created_dt_tm,
                                        'isScholarLoggedIn' => TRUE
                                );
        // echo "<Pre>";print_r($sessionArray);exit();

                $this->session->set_userdata($sessionArray);

                unset($sessionArray['id_scholar'], $sessionArray['isScholarLoggedIn'], $sessionArray['scholar_last_login']);

                $loginInfo = array("id_scholar"=>$result->id, "session_data" => json_encode($sessionArray), "machine_ip"=>$_SERVER['REMOTE_ADDR'], "user_agent"=>getBrowserAgent(), "agent_string"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());

                $uniqueId = rand(0000000000,9999999999);
                $this->session->set_userdata("my_scholar_session_id", md5($uniqueId));


                $this->scholar_login_model->addScholarLastLogin($loginInfo);

                // echo "<Pre>"; print_r($this->session->userdata());exit();
                // echo md5($uniqueId);exit();
                redirect('/scholarship/scheme/list');
                }
            else
            {
                $this->session->set_flashdata('error', 'Email or password mismatch');
                
                $this->index();
            }
        }
    }
}

?>