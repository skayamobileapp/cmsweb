<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Home extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('home.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {            
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            $data['homeList'] = $this->home_model->homeListSearch($formData);

            $this->global['pageTitle'] = 'Website Management System : Home List';
            //print_r($subjectDetails);exit;
            $this->loadViews("home/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('home.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                if($_FILES['image_file'])
                {
                    $certificate_name = $_FILES['image_file']['name'];
                    $certificate_size = $_FILES['image_file']['size'];
                    $certificate_tmp =$_FILES['image_file']['tmp_name'];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                    $image = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
                // echo "<Pre>";print_r($image);exit();

                }

            
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'status' => $status,
                    'created_by' => $user_id
                );

                if($image != '')
                {
                    $data['image'] = $image;
                }
                
                $result = $this->home_model->addNewHome($data);
                redirect('/setup/home/list');
            }
            //print_r($data['stateList']);exit;

            $this->global['pageTitle'] = 'Website Management System : Add Home ';
            $this->loadViews("home/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('home.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/home/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                if($_FILES['image_file'])
                {
                    $certificate_name = $_FILES['image_file']['name'];
                    $certificate_size = $_FILES['image_file']['size'];
                    $certificate_tmp =$_FILES['image_file']['tmp_name'];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                    $image = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
                }

                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'status' => $status,
                    'updated_by' => $user_id
                );

                if($image != '')
                {
                    $data['image'] = $image;
                }
                
                $result = $this->home_model->editHome($data,$id);
                redirect('/setup/home/list');
            }

            $data['home'] = $this->home_model->getHome($id);

            $this->global['pageTitle'] = 'Website Management System : Edit Home ';
            $this->loadViews("home/edit", $this->global, $data, NULL);
        }
    }
}
