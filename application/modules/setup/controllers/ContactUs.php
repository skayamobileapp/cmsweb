<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ContactUs extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('contact_us_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('contact_us.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {            
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            $data['contactUsList'] = $this->contact_us_model->contactUsListSearch($formData);

            $this->global['pageTitle'] = 'Website Management System : Contact Us List';
            //print_r($subjectDetails);exit;
            $this->loadViews("contact_us/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('contact_us.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                if($_FILES['image_file'])
                {
                    $certificate_name = $_FILES['image_file']['name'];
                    $certificate_size = $_FILES['image_file']['size'];
                    $certificate_tmp =$_FILES['image_file']['tmp_name'];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                    $image = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
                // echo "<Pre>";print_r($image);exit();

                }

            
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'status' => $status,
                    'created_by' => $user_id
                );

                if($image != '')
                {
                    $data['image'] = $image;
                }
                
                $result = $this->contact_us_model->addNewContactUs($data);
                redirect('/setup/contactUs/list');
            }
            //print_r($data['stateList']);exit;

            $this->global['pageTitle'] = 'Website Management System : Add Contact Us ';
            $this->loadViews("contact_us/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('contact_us.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/contactUs/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                if($_FILES['image_file'])
                {
                    $certificate_name = $_FILES['image_file']['name'];
                    $certificate_size = $_FILES['image_file']['size'];
                    $certificate_tmp =$_FILES['image_file']['tmp_name'];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                    $image = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
                }

                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'status' => $status,
                    'updated_by' => $user_id
                );

                if($image != '')
                {
                    $data['image'] = $image;
                }
                
                $result = $this->contact_us_model->editContactUs($data,$id);
                redirect('/setup/contactUs/list');
            }

            $data['contactUs'] = $this->contact_us_model->getContactUs($id);

            $this->global['pageTitle'] = 'Website Management System : Edit Contact Us ';
            $this->loadViews("contact_us/edit", $this->global, $data, NULL);
        }
    }
}
