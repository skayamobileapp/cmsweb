<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Projects extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('projects_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('projects.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {            
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            $data['projectsList'] = $this->projects_model->projectsListSearch($formData);

            $this->global['pageTitle'] = 'Website Management System : Projects List';
            //print_r($subjectDetails);exit;
            $this->loadViews("projects/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('projects.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                if($_FILES['image_file'])
                {
                    $certificate_name = $_FILES['image_file']['name'];
                    $certificate_size = $_FILES['image_file']['size'];
                    $certificate_tmp =$_FILES['image_file']['tmp_name'];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                    $image = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
                // echo "<Pre>";print_r($image);exit();

                }

            
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'status' => $status,
                    'created_by' => $user_id
                );

                if($image != '')
                {
                    $data['image'] = $image;
                }
                
                $result = $this->projects_model->addNewProjects($data);
                redirect('/setup/projects/list');
            }
            //print_r($data['stateList']);exit;

            $this->global['pageTitle'] = 'Website Management System : Add Projects ';
            $this->loadViews("projects/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('projects.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/projects/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                if($_FILES['image_file'])
                {
                    $certificate_name = $_FILES['image_file']['name'];
                    $certificate_size = $_FILES['image_file']['size'];
                    $certificate_tmp =$_FILES['image_file']['tmp_name'];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                    $image = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
                }

                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'status' => $status,
                    'updated_by' => $user_id
                );

                if($image != '')
                {
                    $data['image'] = $image;
                }
                
                $result = $this->projects_model->editProjects($data,$id);
                redirect('/setup/projects/list');
            }

            $data['projects'] = $this->projects_model->getProjects($id);

            $this->global['pageTitle'] = 'Website Management System : Edit Projects ';
            $this->loadViews("projects/edit", $this->global, $data, NULL);
        }
    }
}
