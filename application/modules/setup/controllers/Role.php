<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Role extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('role_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('role.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // $data['departmentList'] = $this->department_model->departmentList();
            // $data['staffList'] = $this->staff_model->staffList();
            
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = $this->security->xss_clean($this->input->post('status'));

            $data['searchParam'] = $formData;
            $data['roleList'] = $this->role_model->roleListSearch($formData);

            $this->global['pageTitle'] = 'Examination Management System : Role List';
            //print_r($subjectDetails);exit;
            $this->loadViews("role/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('role.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
            
                $role = $this->security->xss_clean($this->input->post('role'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'role' => $role,
                    'status' => $status
                );
                
                $result = $this->role_model->addNewRole($data);
                redirect('/setup/role/edit/'. $result);
            }
            //print_r($data['stateList']);exit;

            $this->global['pageTitle'] = 'Examination Management System : Add Role';
            $this->loadViews("role/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('role.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/role/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                $role = $this->security->xss_clean($this->input->post('role'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'role' => $role,
                    'status' => $status
                );
                
                $result = $this->role_model->editRole($data,$id);
                redirect('/setup/role/list');
            }

            $data['role'] = $this->role_model->getRole($id);

            $this->global['pageTitle'] = 'Examination Management System : Edit Role';
            $this->loadViews("role/edit", $this->global, $data, NULL);
        }
    }
}
