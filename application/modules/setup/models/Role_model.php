<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Role_model extends CI_Model
{
    function roleListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('roles as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function roleListSearch($data)
    {
        $this->db->select('c.*');
        $this->db->from('roles as c');
        if ($data['name'] != '')
        {
            $likeCriteria = "(c.role  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['status'] != '')
        {
            $this->db->where('c.status', $data['status']);
        }
        $this->db->order_by("c.id", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

     function getRole($id)
    {
         $this->db->select('c.*');
        $this->db->from('roles as c');
        // $this->db->join('department as d', 'c.id_department = d.id','left');
        // $this->db->join('staff as s', 'c.id_staff_coordinator = s.id','left');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();
        // echo "<pre>";print_r($result);die;

        return $result;
    }

    function addNewRole($data)
    {
        $this->db->trans_start();
        $this->db->insert('roles', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editRole($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('roles', $data);

        return TRUE;
    }

    function deleteRole($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('roles', $data);

        return $this->db->affected_rows();
    }

    function checkAccess($roleId,$code)
    {
        $this->db->select('permissions.id');
        $this->db->from('role_permissions');
        $this->db->join('permissions', 'role_permissions.id_permission = permissions.id');

        $this->db->where('role_permissions.id_role', $roleId);
        $this->db->where('code', $code);
        $query = $this->db->get();
        if(empty($query->row())){
            return 1;
        }
        else{
            return 0;
        }
    }
}
