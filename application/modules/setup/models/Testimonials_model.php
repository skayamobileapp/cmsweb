<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Testimonials_model extends CI_Model
{

    function testimonialsListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('testimonials_setup as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function testimonialsListSearch($data)
    {
        $this->db->select('c.*');
        $this->db->from('testimonials_setup as c');
        if (!empty($data['name']))
        {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%' or c.name_optional_language  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

     function getTestimonials($id)
    {
        $this->db->select('c.*');
        $this->db->from('testimonials_setup as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function addNewTestimonials($data)
    {
        $this->db->trans_start();
        $this->db->insert('testimonials_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editTestimonials($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('testimonials_setup', $data);

        return TRUE;
    }

    function deleteTestimonials($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('testimonials_setup', $data);
        return $this->db->affected_rows();
    }
}
