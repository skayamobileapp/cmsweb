<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contact_us_model extends CI_Model
{

    function contactUsListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('contact_us_setup as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function contactUsListSearch($data)
    {
        $this->db->select('c.*');
        $this->db->from('contact_us_setup as c');
        if (!empty($data['name']))
        {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%' or c.name_optional_language  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

     function getContactUs($id)
    {
        $this->db->select('c.*');
        $this->db->from('contact_us_setup as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function addNewContactUs($data)
    {
        $this->db->trans_start();
        $this->db->insert('contact_us_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editContactUs($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('contact_us_setup', $data);

        return TRUE;
    }

    function deleteContactUs($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('contact_us_setup', $data);
        return $this->db->affected_rows();
    }
}
