<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Projects_model extends CI_Model
{

    function projectsListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('projects_setup as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function projectsListSearch($data)
    {
        $this->db->select('c.*');
        $this->db->from('projects_setup as c');
        if (!empty($data['name']))
        {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%' or c.name_optional_language  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

     function getProjects($id)
    {
        $this->db->select('c.*');
        $this->db->from('projects_setup as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function addNewProjects($data)
    {
        $this->db->trans_start();
        $this->db->insert('projects_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editProjects($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('projects_setup', $data);

        return TRUE;
    }

    function deleteProjects($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->delete('projects_setup');
        return $this->db->affected_rows();
    }
}
