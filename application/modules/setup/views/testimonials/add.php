<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Testimonials</h3>
        </div>

        
        <form id="form_data" method="post" enctype="multipart/form-data">

        <div class="form-container">
            <h4 class="form-group-title">Testimonials Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Upload Image <span class='error-text'>*</span></label>
                        <input type="file" class="form-control" id="image_file" name="image_file" >
                    </div>
                </div>



               <!--  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name Other Language</label>
                        <input type="text" class="form-control" id="name_in_malay" name="name_in_malay">
                    </div>
                </div>
            </div>

            <div class="row"> -->

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                        </label>                              
                    </div>                         
                </div>

            </div>

        </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>
<script type="text/javascript">

    $('select').select2();

    $(document).ready(function()
    {
        $("#form_data").validate(
        {
            rules:
            {
                name:
                {
                    required: true
                },
                image_file:
                {
                    required: true
                }
            },
            messages:
            {
                name:
                {
                    required: "<p class='error-text'>Name Required</p>",
                },
                image_file:
                {
                    required: "<p class='error-text'>Select Image</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>