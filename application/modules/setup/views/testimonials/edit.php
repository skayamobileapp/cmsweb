<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
            <h3>Edit Testimonials </h3>
        </div>

        <div class="form-container">
        <h4 class="form-group-title">Testimonials Details</h4>

        <form id="form_data" action="" method="post" enctype="multipart/form-data">


        <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Name <span class='error-text'>*</span></label>
                    <input type="text" class="form-control" id="name" name="name" value="<?php echo $testimonials->name;?>">
                </div>
            </div>

            <div class="col-sm-4">
                    <div class="form-group">
                        <label>Upload Image 

                            <?php 
                        if(empty($testimonials->image))
                        {
                        ?>
                            <span class='error-text'>*</span>

                        <?php 

                        }
                        ?>


                        </label>
                        <?php 
                        if($testimonials->image)
                        {
                        ?>
                            <a href="<?php echo '/assets/images/' . $testimonials->image; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $testimonials->image; ?>)" title="<?php echo $testimonials->image; ?>">
                                            View
                                        </a>
                        <?php 

                        }
                        ?>

                        <input type="file" class="form-control" id="image_file" name="image_file" >
                    </div>
                </div>

            
            <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" <?php if($testimonials->status=='1') {
                             echo "checked=checked";
                          };?>><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0" <?php if($testimonials->status=='0') {
                             echo "checked=checked";
                          };?>>
                          <span class="check-radio"></span> In-Active
                        </label>                              
                    </div>                         
            </div>

        </div>


    </div>

    <div class="button-block clearfix">
        <div class="bttn-group">
            <button type="submit" class="btn btn-primary btn-lg">Save</button>
            <a href="../list" class="btn btn-link">Cancel</a>
        </div>
    </div>


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>


<script type="text/javascript">
    $('select').select2();

    $(document).ready(function()
    {
        $("#form_data").validate(
        {
            rules:
            {
                name:
                {
                    required: true
                }
            },
            messages:
            {
                name:
                {
                    required: "<p class='error-text'>Name Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>