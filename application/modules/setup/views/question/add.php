<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Question</h3>
        </div>

        
        <form id="form_data" action="" method="post" enctype="multipart/form-data">

        <div class="form-container">
            <h4 class="form-group-title">Question Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Question <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course <span class='error-text'>*</span></label>
                        <select name="id_course" id="id_course" class="form-control" style="width: 408px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseList))
                            {
                                foreach ($courseList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Topic <span class='error-text'>*</span></label>
                        <select name="id_topic" id="id_topic" class="form-control" style="width: 408px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($topicList))
                            {
                                foreach ($topicList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course Learning Object <span class='error-text'>*</span></label>
                        <select name="id_course_learning_objective" id="id_course_learning_objective" class="form-control" style="width: 408px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseLearningObjectList))
                            {
                                foreach ($courseLearningObjectList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bloom Taxonomy <span class='error-text'>*</span></label>
                        <select name="id_bloom_taxonomy" id="id_bloom_taxonomy" class="form-control" style="width: 408px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($bloomTaxonomyList))
                            {
                                foreach ($bloomTaxonomyList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Difficult Level <span class='error-text'>*</span></label>
                        <select name="id_difficult_level" id="id_difficult_level" class="form-control" style="width: 408px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($difficultLevelList))
                            {
                                foreach ($difficultLevelList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Upload Image</label>
                        <input type="file" class="form-control" id="image_file" name="image_file" >
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                        </label>                              
                    </div>                         
                </div>

            </div>

        </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>
<script type="text/javascript">

    $('select').select2();

    $(document).ready(function()
    {
        $("#form_data").validate(
        {
            rules:
            {
                description:
                {
                    required: true
                },
                id_course:
                {
                    required: true
                },
                id_topic:
                {
                    required: true
                },
                id_course_learning_objective:
                {
                    required: true
                },
                id_bloom_taxonomy:
                {
                    required: true
                },
                id_difficult_level:
                {
                    required: true
                }
            },
            messages:
            {
                description:
                {
                    required: "<p class='error-text'>Question Required</p>",
                },
                id_course:
                {
                    required: "<p class='error-text'>Select Course</p>",
                },
                id_topic:
                {
                    required: "<p class='error-text'>Select Topic</p>",
                },
                id_course_learning_objective:
                {
                    required: "<p class='error-text'>Select Course Learining Object</p>",
                },
                id_bloom_taxonomy:
                {
                    required: "<p class='error-text'>Select Bloom Taxonomy</p>",
                },
                id_difficult_level:
                {
                    required: "<p class='error-text'>Select Difficult Level</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>