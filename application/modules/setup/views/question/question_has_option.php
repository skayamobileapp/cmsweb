<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
            <h3>Add Options </h3>
        </div>
        

        <div class="form-container">
        <h4 class="form-group-title">Question Details</h4>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Question <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $question->description;?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course <span class='error-text'>*</span></label>
                        <select name="id_course" id="id_course" class="form-control" style="width: 408px" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseList))
                            {
                                foreach ($courseList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $question->id_course)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Topic <span class='error-text'>*</span></label>
                        <select name="id_topic" id="id_topic" class="form-control" style="width: 408px" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($topicList))
                            {
                                foreach ($topicList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $question->id_course)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course Learning Object <span class='error-text'>*</span></label>
                        <select name="id_course_learning_objective" id="id_course_learning_objective" class="form-control" style="width: 408px" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseLearningObjectList))
                            {
                                foreach ($courseLearningObjectList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $question->id_course_learning_objective)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bloom Taxonomy <span class='error-text'>*</span></label>
                        <select name="id_bloom_taxonomy" id="id_bloom_taxonomy" class="form-control" style="width: 408px" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($bloomTaxonomyList))
                            {
                                foreach ($bloomTaxonomyList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $question->id_bloom_taxonomy)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Difficult Level <span class='error-text'>*</span></label>
                        <select name="id_difficult_level" id="id_difficult_level" class="form-control" style="width: 408px" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($difficultLevelList))
                            {
                                foreach ($difficultLevelList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $question->id_difficult_level)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Upload Image</label>

                        <?php 
                        if($question->image)
                        {
                        ?>
                            <br>
                            <a href="<?php echo '/assets/images/' . $question->image; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $question->image; ?>)" title="<?php echo $question->image; ?>">
                                            View
                                        </a>
                        <?php 

                        }else
                        {

                            echo "<br> <a title='No File'> No File Uploaded </a>";
                        }
                        ?>
                    </div>
                </div>


                

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($question->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($question->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>

            </div>


        </div>

    <div class="button-block clearfix">
        <div class="bttn-group">
            <button type="submit" class="btn btn-primary btn-lg">Save</button>
            <a href="../list" class="btn btn-link">Back</a>
        </div>
    </div>
    


        <div class="form-container">
            <h4 class="form-group-title"> Option Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Options</a>
                    </li>
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">




                        <form id="form_comitee" action="" method="post" enctype="multipart/form-data">


                                <br>

                                <div class="row">

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Option <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="option_description" name="option_description">
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <p>Is Correct Answer <span class='error-text'>*</span></p>
                                            <label class="radio-inline">
                                              <input type="radio" name="is_correct_answer" id="is_correct_answer" value="0" checked="checked"><span class="check-radio"></span> No
                                            </label>

                                            <label class="radio-inline">
                                              <input type="radio" name="is_correct_answer" id="is_correct_answer" value="1"><span class="check-radio"></span> Yes
                                            </label>
                                        </div>                         
                                    </div>



                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Upload Image</label>
                                            <input type="file" class="form-control" id="image" name="image" >
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <button type="submit" class="btn btn-primary btn-lg form-row-btn">Add</button>
                                    </div>
                                
                                </div>


                        </form>


                        <?php

                            if(!empty($questionHasOption))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Comitee Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Option</th>
                                                 <th class="text-center">Is Correct</th>
                                                 <th class="text-center">Image</th>
                                                 <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($questionHasOption);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $questionHasOption[$i]->option_description;?></td>
                                                <td style="text-align: center;"><?php if($questionHasOption[$i]->is_correct_answer == '1')
                                                  {
                                                    echo "Yes";
                                                  }
                                                  else
                                                  {
                                                    echo "No";
                                                  } 
                                                  ?></td>

                                                <td class="text-center">
                                                <?php 
                                                if($questionHasOption[$i]->image)
                                                {
                                                ?>
                                                <a href="<?php echo '/assets/images/' . $questionHasOption[$i]->image; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $questionHasOption[$i]->image; ?>)" title="<?php echo $questionHasOption[$i]->image; ?>">
                                                    View
                                                </a>
                                                <?php
                                                }else
                                                {
                                                    echo "<a title='No File '> No File Uploaded </a>";
                                                }
                                                ?>

                                                </td>


                                                <td class="text-center">
                                                <a onclick="deleteOption(<?php echo $questionHasOption[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div> 
                    </div>




                </div>

            </div>
        </div> 



    


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>


<script type="text/javascript">
    $('select').select2();

    function deleteOption(id)
    {
            // alert(id);
            $.ajax(
            {
               url: '/setup/question/deleteOption/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // alert(result);
                    window.location.reload();
               }
            });
    }



    $(document).ready(function()
    {
        $("#form_comitee").validate(
        {
            rules:
            {
                option_description:
                {
                    required: true
                },
                is_correct_answer:
                {
                    required: true
                }
            },
            messages:
            {
                option_description:
                {
                    required: "<p class='error-text'>Option Required</p>",
                },
                is_correct_answer:
                {
                    required: "<p class='error-text'>Select Is Correct / Not</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>