            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="#" class="user-profile-link">
                        <span><img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg"></span> <?php echo $name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <!-- <li><a href="/sponser/user/profile">Edit Profile</a></li> -->
                            <li ><a href="/livechat/php/app.php?admin" target="_blank">Chat</a></li>
                            <li><a href="/setup/user/logout">Logout</a></li>
                        </ul>
                    </div>
                   
                    </div>
                    <div class="sidebar-nav">
                   <!--  <h4>General Setup</h4>
                        <ul>
                            <li><a href="/setup/role/list">Roles</a></li>
                            <li><a href="/setup/user/list">User</a></li>
                    </ul> -->
                    <h4>Setup</h4>
                    <ul>
                        <li><a href="/setup/home/list">Home</a></li>
                        <li><a href="/setup/projects/list">Projects</a></li>
                        <li><a href="/setup/contactUs/list">Contact Us</a></li>
                        <li><a href="/setup/testimonials/list">Testimonials</a></li>
                        </ul>                   
                </div>
            </div>
            