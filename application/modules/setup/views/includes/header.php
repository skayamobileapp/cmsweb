<!DOCTYPE html>
<html lang="en"> 
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $pageTitle; ?></title>
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/datatable.min.css">

    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/main.css">
</head>
<body>
    <header class="navbar navbar-default navbar-fixed-top main-header">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#" style="font-size:14px;">Examination Management System</a>
            </div>

            <nav class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <!-- <li ><a href="/livechat/php/app.php?admin" target="popup" onclick="window.open('/livechat/php/app.php?admin','Chat','width=600,height=400')">Open page in new window</a></li > -->
                    <!-- <li  class="active"><a href="/setup/user/list">Setup</a></li> -->
                    <!-- <li><a href="/exam/examName/list">Exam</a></li> -->
                    <!-- <li><a href="/setup/user/logout">logout</a></li> -->
                </ul>
                
            </nav>
        </div>
    </header>
</body>

     
