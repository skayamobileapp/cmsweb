<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List of Projects Content</h3>
      <a href="add" class="btn btn-primary">+ Add Projects Content</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
               Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">

                  <div class="col-sm-6">
                   
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Projects Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name"  id="name" value="<?php echo $searchParam['name'] ?>">
                      </div>
                    </div>
                  </div>
                </div>
                
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Projects Name</th>
            <th class="text-center">Image</th>
            <!-- <th>Name In Other Language</th> -->
            <th class="text-center">Status</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($projectsList))
          {
            $i=1;
            foreach ($projectsList as $record)
            {
             ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->name; ?></td>
                <td class="text-center">
                  <?php 
                  if($record->image)
                  {
                  ?>
                  <a href="<?php echo '/assets/images/' . $record->image; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->image; ?>)" title="<?php echo $record->image; ?>">
                      View
                  </a>
                  <?php
                  }else
                  {
                      echo "<a title='No File '> No File Uploaded </a>";
                  }
                  ?>

                </td>
                <td style="text-align: center;"><?php if( $record->status == '1')
                  {
                    echo "Active";
                  }
                  else
                  {
                    echo "In-Active";
                  } 
                  ?></td>
                <!-- // <td><?php echo $record->name_optional_language; ?></td> -->
                <td class="text-center">

                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">
                    Edit
                  </a>
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    $('select').select2();
    
    function clearSearchForm()
    {
      window.location.reload();
    }
</script>